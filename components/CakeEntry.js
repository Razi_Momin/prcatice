import React, { useState, useLayoutEffect } from 'react'
import { StyleSheet, Text, View, TextInput, Button, ToastAndroid, Alert, ScrollView } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label } from 'native-base';
import DateTimePicker from '@react-native-community/datetimepicker';
import { webService } from '../webService';
const CakeEntry = ({ navigation }) => {
    const [count, setCount] = useState(0);
    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <View style={{ marginRight: 15 }}>
                    <Button onPress={() => navigation.navigate('Details')} title="Show Details" />
                </View>
            ),
        });
    }, [navigation, setCount]);

    const [dateValidate, setDateValidate] = useState(Date.parse(new Date()));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [clientDate, setClientDate] = useState('');
    const [cakeName, setCakeName] = useState('');
    const [cakeWeight, setCakeWeight] = useState(0);
    const [customerName, setCustomerName] = useState('');
    const [price, setPrice] = React.useState(0);
    const onChange = (event, selectedDate) => {
        console.log(selectedDate);
        const currentDate = selectedDate || dateValidate;
        setShow(Platform.OS === 'ios');
        var dateObj = currentDate;
        var month = dateObj.getMonth() + 1; //months from 1-12
        var day = dateObj.getDate();
        var year = dateObj.getFullYear();
        var hours = new Date().getHours();
        var minutes = new Date().getMinutes();
        var seconds = new Date().getSeconds();
        hours = ("0" + hours).slice(-2);
        // newdate = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
        newdate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
        setClientDate(newdate);
        setDateValidate(Date.parse(new Date()));
        // console.log(newdate + "fefwefwew");
    };
    const resetForm = () => {
        setCakeName('');
        setClientDate('');
        setCakeWeight('');
        setCustomerName('');
        setPrice(0);
    }
    const submitForm = () => {
        let errorMessage;
        let validation = false;
        if (cakeName === '') {
            errorMessage = 'Please enter cake Name'
        } else if (cakeWeight === '') {
            errorMessage = 'Please enter cake weight'
        } else if (customerName === '') {
            errorMessage = 'Please enter customer name'
        } else if (price === '') {
            errorMessage = 'Please enter price'
        } else if (clientDate === '') {
            errorMessage = 'Please enter date'
        } else {
            validation = true;
            callApi();
        }
        !validation ? ToastAndroid.show(errorMessage, ToastAndroid.TOP) : null;
        // !validation ? Alert.alert('ALERT', errorMessage) : null;
    }
    const callApi = () => {
        let postArray = { cakeName, cakeWeight, customerName, price, clientDate };
        let apiEndpoint = 'forms/getRecords';
        let header = webService.getHeaders({})
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            console.log(response);
            if (response.status === 200) {
                response = response.data;
                if (response.success) {
                    resetForm();
                    Alert.alert('Success', response.message);
                    setTimeout(() => {
                        navigation.navigate('Details');
                    }, 2000);

                    // Alert.alert('Success', response.message)
                } else {
                    Alert.alert('ERROR', response.message)
                }
            } else {
                Alert.alert('ERROR', 'Net is not working')
            }
        })
    }

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };


    return (
        <ScrollView>
            <View style={{ paddingHorizontal: 10, backgroundColor: '#fff' }}>
                <View style={styles.container}>
                    <Item fixedLabel last>
                        <Label>Cake Name</Label>
                        <Input onChangeText={text => setCakeName(text)}
                            value={cakeName} />
                    </Item>
                </View>
                <View style={styles.container}>
                    <Item fixedLabel last>
                        <Label>Cake Weight</Label>
                        <Input keyboardType={'numeric'}
                            onChangeText={tel => setCakeWeight(tel)}
                            value={String(cakeWeight)} />
                    </Item>
                </View>
                <View style={styles.container}>
                    <Item fixedLabel last>
                        <Label>Customer Name</Label>
                        <Input keyboardType={'numeric'}
                            onChangeText={text => setCustomerName(text)}
                            value={customerName} />
                    </Item>
                </View>

                <View style={styles.container}>
                    <Item fixedLabel last>
                        <Label>Price</Label>
                        <Input keyboardType={'numeric'}
                            onChangeText={tel => setPrice(tel)}
                            value={String(price)} />
                    </Item>
                </View>
                <Item fixedLabel last>
                    <Label>Date</Label>
                    <Input
                        onFocus={showDatepicker}
                        value={clientDate} />
                </Item>

                {/* <View style={styles.container}>
                    <Text>Cake Name
                      </Text>
                    <TextInput
                        style={styles.inputBox}
                        onChangeText={text => setCakeName(text)}
                        value={cakeName}
                    />
                </View> */}
                {/* <View style={styles.container}>
                    <Text>Cake Weight</Text>
                    <TextInput keyboardType={'numeric'}
                        style={styles.inputBox}
                        onChangeText={tel => setCakeWeight(tel)}
                        value={String(cakeWeight)}
                    />
                </View> */}
                {/* <View style={styles.container}>
                    <Text>Customer Name </Text>
                    <TextInput
                        style={styles.inputBox}
                        onChangeText={text => setCustomerName(text)}
                        value={customerName}
                    />
                </View> */}
                {/* <View style={styles.container}>
                    <Text>Price</Text>
                    <TextInput keyboardType={'numeric'}
                        style={styles.inputBox}
                        onChangeText={tel => setPrice(tel)}
                        value={String(price)}
                    />
                </View> */}
                {/* <View style={styles.container}>
                    <Text>Date</Text>
                    <TextInput
                        style={styles.inputBox}
                        onFocus={showDatepicker}
                        value={clientDate}
                    />
                </View> */}
                <View>
                    {/* <View>
          <Button onPress={showDatepicker} title="Show date picker!" />
        </View> */}

                    {show && (
                        <DateTimePicker
                            testID="dateTimePicker"
                            value={dateValidate}
                            mode={mode}
                            is24Hour={true}
                            display="calendar"
                            onChange={onChange}
                        />
                    )}
                </View>
                <Button onPress={submitForm}
                    title="Submit"
                    color="purple"
                    accessibilityLabel="Learn more about this purple button"
                    height='40'
                />
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
        // backgroundColor: '#fff',
        // paddingHorizontal: 15
    },
    inputBox: {
        backgroundColor: '#fff'
    }
});
export default CakeEntry

