import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Alert } from 'react-native'
import { webService } from '../webService';
const CakeDetails = () => {
    const [productDetails, setProductDetails] = useState([]);
    const [totalCount, setTotalCount] = useState();
    const [totalRevenue, setTotalRevenue] = useState(0);
    useEffect(() => {
        let apiEndpoint = 'forms/getRecords';
        let header = webService.getHeaders({})
        //console.log(postArray)
        webService.get(apiEndpoint, header).then((response) => {
            console.log(response);
            if (response.status === 200) {
                response = response.data;
                if (response.success) {
                    setProductDetails(response.data.data);
                    console.log(productDetails);
                } else {
                    Alert.alert('ERROR', response.message)
                }
            } else {
                Alert.alert('ERROR', 'Net is not working')
            }
        })
    }, [])
    return (
        <View>
            <Text>Details</Text>
        </View>
    )
}

export default CakeDetails

const styles = StyleSheet.create({})
