import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CakeEntry from './components/CakeEntry'
import CakeDetails from './components/CakeDetails'
const Stack = createStackNavigator();
const App = (props) => {
  // console.log({ navigation });
  return (
    // <View>
    //   <Text>efef</Text>
    // </View>


    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen options={{
          title: 'Entry Page'
        }} name="EntryPage" component={CakeEntry} />
        <Stack.Screen options={{ title: 'Details Page' }} name="Details" component={CakeDetails} />
      </Stack.Navigator>
    </NavigationContainer>

  )
}

function DetailsScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
    </View>
  );
}
export default App

const styles = StyleSheet.create({})
